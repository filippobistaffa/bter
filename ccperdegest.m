function ccpdb = ccperdegest(G,bins,nsamples)
%CCPERDEGEST Estimate of mean clustering coefficient per degree bin.
%
%   CCPD = CCPERDEGEST(G,B,N) computes the per-degree-bin clustering
%   coefficient, i.e., CCPD(k) is the mean clustering coefficient for nodes
%   in degree bin k. The graph G is assumed to be undirected, unweighted,
%   and to contain no self edges. This is *not* checked by the code. The
%   vector B gives the bin boundaries, see HISTC. The computation is
%   approximate, using wedge sampling.  
%
%   NOTE: This is an interface to the MEX function provided by
%   ccperdegest_mex.c.
%
%   See also CCPERDEG, BINDATA.
%
%FEASTPACK v1.0, Sandia National Laboratories, January 2013

% Sandia National Laboratories is a multi-program laboratory managed and
% operated by Sandia Corporation, a wholly owned subsidiary of Lockheed
% Martin Corporation, for the U.S. Department of Energy's National Nuclear
% Security Administration under contract DE-AC04-94AL85000. 


 
ccpdb = ccperdegest_mex(G,bins,nsamples);   


function G = edges2graph(E,nnodes)
%EDGES2GRAPH Create an undirected, simple graph from edge list.
%
%   G = EDGES2GRAPH(E) creates an adjaceny matrix for the graph where
%   E(k,1) and E(k,2) specifies the kth edge. All edges are treated as
%   undirected. Duplicate edges are removed. Loops are removed.
%
%   G = EDGES2GRAPH(E,N) specifies the number of number of nodes in the
%   graph.  
%
%FEASTPACK v1.0, Sandia National Laboratories, February 2013.

% Sandia National Laboratories is a multi-program laboratory managed and
% operated by Sandia Corporation, a wholly owned subsidiary of Lockheed
% Martin Corporation, for the U.S. Department of Energy's National Nuclear
% Security Administration under contract DE-AC04-94AL85000. 

if ~exist('nnodes','var')
    nnodes = max(E(:));
else
    % Error checking only
    tmp = max(E(:));
    if (tmp > nnodes)
        fprintf('Highest index in E is %d, but N = %d', tmp, nnodes);
    end
end

ii = E(:,1);
jj = E(:,2);
G = spones(sparse([ii;jj],[jj;ii],1,nnodes,nnodes));
G = spdiags(zeros(nnodes,1),0,G);

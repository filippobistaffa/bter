function [t,d,w] = tricnt(G,d,matlabbgl)
%TRICNT Count number of triangles per vertex in a simple, undirected graph
%
%   T = TRICNT(G) takes a sparse adjacency matrix G and computes the number
%   of triangles per vertex. Note taht there is no error checking on G. It
%   is up to the user to ensure that G is symmetric, has only 0/1 entries
%   (but *not* binary), and has no entries on the diagonal.
%
%   T = TRICNT(G,D) takes a second argument which is the degree per vertex
%   and does not recalculate it.
%
%   T = TRICNT(G,D,true) uses the clustering_coefficients from MATLAB_BGL.
%   This assumes that this package is installed and in the path.
%
%   [T,D,W] = TRICNT(G) also returns the degree and number of wedges per
%   vertex.
%
%   NOTE: This is an interface to the MEX function provided by
%   tricnt_mex.c, unless the clustering_coefficients function from
%   MATLAB_BGL is used.
% 
%   See also CCPERDEG.
%
%FEASTPACK v1.0, Sandia National Laboratories, January 2013

% Sandia National Laboratories is a multi-program laboratory managed and
% operated by Sandia Corporation, a wholly owned subsidiary of Lockheed
% Martin Corporation, for the U.S. Department of Energy's National Nuclear
% Security Administration under contract DE-AC04-94AL85000. 


if ~exist('matlabbgl','var')
    matlabbgl = false;
end

if ~exist('d','var') || isempty(d)
    d = full(sum(G,2));
end

w = d.*(d-1)/2;

if (matlabbgl)
    
    if ~exist('clustering_coefficients.m','file')
        error('Must install MATLAB_BGL toolbox');
    end
    options.undirected = 1;
    options.unweighted = 1;
    cc = clustering_coefficients(G,options);
    t = round(w.*cc);
   
else
    
    t = tricnt_mex(G);

end


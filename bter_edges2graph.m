function [G,G1,G2] = bter_edges2graph(E1,E2)
%BTER_EDGES2GRAPH Create a graph from edge lists.
%
%   G = BTER_EDGES2GRAPH(E1,E2) returns a sparse adjancency matrix
%   corresponding to the given edge lists produced by BTER. The graph is
%   undirected, unweighted, and has no loops, even if E1 and E2 contain
%   these. 
%
%   [G,G1,G2] = BTER_EDGES2GRAPH(E1,E2) returns the graphs corresponding to
%   Phase 1 and Phase 2 in addition to the combined graph.
%
%   See also BTER, EDGES2GRAPH
%
%FEASTPACK v1.0, Sandia National Laboratories, April 2013.

% Sandia National Laboratories is a multi-program laboratory managed and
% operated by Sandia Corporation, a wholly owned subsidiary of Lockheed
% Martin Corporation, for the U.S. Department of Energy's National Nuclear
% Security Administration under contract DE-AC04-94AL85000. 

if isempty(E1)
    nnodes = max(E2(:));
elseif isempty(E2)
    nnodes = max(E1(:));
else
    nnodes = max(max(E1(:)),max(E2(:)));
end

if (nargout < 3)
    G = edges2graph([E1;E2],nnodes);
else    
    G1 = edges2graph(E1,nnodes);    
    G2 = edges2graph(E2,nnodes);    
    G = spones(G1+G2);
    G = spdiags(zeros(nnodes,1),0,G);
end


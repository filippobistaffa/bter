function cn(nnodes, maxdeg_bound, avgdeg_target, maxccd_target, gcc_target, seed, graphcsv, statscsv)

rng(seed);
nedges_target= nnodes*avgdeg_target/2;
meq=(-sqrt(-4*avgdeg_target*nnodes+4*nnodes^2-4*nnodes+1)+2*nnodes-1)/2;
graphname = 'test';
fprintf('Input parameters:\nn = %d\n', nnodes);
fprintf('Average degree = %.5f\n', avgdeg_target);
fprintf('Global clustering coefficient = %.5f\n', gcc_target);
fprintf('Equivalent m = %.5f\n', meq);
fprintf('Edges = %d\n', nedges_target);
fprintf('Seed = %d\n\n', seed);

% Run the optimization procedure
tau = 1e-3 / nnodes; % tau << 1/nnodes
[alpha, beta] = degdist_param_search(avgdeg_target, maxdeg_bound, 'maxdeg_prbnd', tau);
%fprintf('Selected alpha=%f and beta=%f\n', alpha, beta);

% Create the discrete PDF
pdf = dglnpdf(maxdeg_bound,alpha,beta);
nd = gendegdist(nnodes,pdf);

% Find the optimal xi
maxdeg = find(nd>0,1,'last');
xi = cc_param_search(nd, maxccd_target, gcc_target);
%fprintf('Selected xi=%f\n', xi);

% Plot the target clustering coefficient per degree (CCD)
ccd_target = [0; maxccd_target * exp(-(0:maxdeg-2)'.* xi)];

% Compute target CCD
ccd_stddev = min(0.01,ccd_target/2);
ccd = max(ccd_target +  randn(size(ccd_target)) .* ccd_stddev,0);

fprintf('Running BTER...\n');
t1=tic;
[E1,E2] = bter(nd,ccd,'rngseed',seed);
toc(t1)
fprintf('Number of edges created by BTER: %d\n', size(E1,1) + size(E2,1));

%fprintf('Turning edge list into adjacency matrix (including dedup)...\n');
t2=tic;
G_bter = bter_edges2graph(E1,E2);
toc(t2);
%fprintf('Number of edges in dedup''d graph: %d\n', nnz(G_bter)/2);

% Number of nodes and edges
nnodes_bter = size(G_bter,1);
nedges = sum(nd .* (1:length(nd))');
nedges_bter = nnz(G_bter)/2;
fprintf('\nFinal parameters\n', graphname);
fprintf('Number of nodes: %d\n', nnodes_bter);
fprintf('Number of edges: %d\n', nedges_bter);

% Degree disttribution
nd_bter = accumarray(nonzeros(sum(G_bter,2)),1);
maxdeg_bter = find(nd_bter>0,1,'last');
fprintf('Maximum degree: %d\n', maxdeg_bter);

% Clustering coefficients
[ccd_bter,gcc_bter] = ccperdeg(G_bter);
fprintf('Global clustering coefficient: %.5f\n', gcc_bter);
ad_bter=2*nedges_bter/nnodes_bter;
fprintf('Average degree = %.5f\n', ad_bter);
meq_bter=(-sqrt(-4*ad_bter*nnodes_bter+4*nnodes_bter^2-4*nnodes_bter+1)+2*nnodes_bter-1)/2;
fprintf('Equivalent m = %.5f\n', meq_bter);

% Save edges to file
[i,j]=find(G_bter);
fid = fopen(graphcsv,'w');
for k=1:length(i)
if i(k) > j(k)
fprintf(fid, '%d,%d\n', i(k)-1, j(k)-1);
end
end
fclose(fid);

% Save stats
fid=fopen(statscsv,'w');
%fprintf(fid,'%d,%d,%.5f,%.5f,%.5f\n', nnodes, nedges_target, avgdeg_target, meq, gcc_target);
fprintf(fid,'%d,%d,%d,%.5f,%.5f,%.5f\n', seed, nnodes_bter, nedges_bter, ad_bter, meq_bter, gcc_bter);
fclose(fid);

exit

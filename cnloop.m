function cnloop(nnodes, avgdeg_input, avgdeg_target, tol, gcc_target, n, folder)
for i=1:n
	ratio = 1;
	while (ratio)
		seed = floor(rand * 100000000);
		try
			ratio = cnt(nnodes, avgdeg_input, avgdeg_target, tol, gcc_target, seed, ...
			fullfile(folder, strcat(num2str(seed), '.csv')), fullfile(folder, strcat(num2str(seed), '.stats')));
			if (ratio) avgdeg_input = avgdeg_input*ratio;
			end
		end
	end
end

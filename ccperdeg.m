function [cd,gcc,info] = ccperdeg(G,varargin)
%CCPERDEG Mean clustering coefficient per degree
%
%   CD = CCPERDEG(G) computes the per-degree clustering coefficient, i.e.,
%   CD(d) is the mean clustering coefficient for nodes of degree d. If bins
%   are used, CD(d) returns the clustering coefficient for the bin
%   containing degree d.
%   
%   [CD,GCC] = CCPERDEG(G) also returns the global clustering coefficient.
%
%   [CD,GCC,INFO] = CCPERDEG(G) also returns additional information.
%
%   [...] = CCPERDEG(G,'param',value accepts parameter-value pairs:
%
%   - 'nsamples'  - Number of samples to use. Set to zero for exact
%                   calcuation. Default: 0
%   - 'bins'      - Specify the degree bins for binned data. Default: []
%   - 'tau'       - Specify tau-value for binning. Default: []
%   - 'omega'     - Specify omega-value for binning. Default: []
%   - 'matlabbgl' - Specify use of MATLAB-BGL clusteringcoefficients
%                   function rather than included code. Default: false
%
%   Note that the 'bins' parameters overrides the 'tau' and 'omega'
%   specifications. Otherwise, both 'tau' and 'omega' must be specified to
%   create bins.
%
%   See also TRICNT, BINDATA.
%
%FEASTPACK v1.0, Sandia National Laboratories, May 2012, Last Updated February 2013

% Sandia National Laboratories is a multi-program laboratory managed and
% operated by Sandia Corporation, a wholly owned subsidiary of Lockheed
% Martin Corporation, for the U.S. Department of Energy's National Nuclear
% Security Administration under contract DE-AC04-94AL85000. 

% ** Process inputs
params = inputParser;
params.addParamValue('nsamples', 0);
params.addParamValue('bins',[]);
params.addParamValue('tau', []);
params.addParamValue('omega', []);
params.addParamValue('matlabbgl', false);
params.parse(varargin{:});

nsamples = params.Results.nsamples;
bins = params.Results.bins;
tau = params.Results.tau;
omega = params.Results.omega;
matlabbgl = params.Results.matlabbgl;


% ** Create bins
d = full(sum(G,2));
maxd = max(d);

if isempty(bins)
    if isempty(omega) || isempty(tau)
        bins = (1:(maxd+1))';
    else
        nbins = binlookup(maxd+1,omega,tau); 
        bins = binstart((1:(nbins+1))',omega,tau);
    end
end

% **
if nsamples == 0
    
    [t,d,w] = tricnt(G,d,matlabbgl);             
    [~,binId] = histc(d,bins);
    tf = binId > 0;
    binWedges = accumarray(binId(tf),w(tf));
    nbins = length(binWedges);
    binTriangles = accumarray(binId(tf),t(tf),[nbins 1]);
    cdb = binTriangles ./ max(1,binWedges);   
    gcc = sum(t)/sum(w);

else    
   cdb = ccperdegest(G,bins,nsamples); 
   [~,binId] = histc(d,bins);
   tf = binId > 0;
   w = d.*(d-1)/2;
   binWedges = accumarray(binId(tf),w(tf),size(cdb));
   gcc = (binWedges'*cdb) / sum(binWedges);
   t = [];
   binTriangles = [];
end

[~,binId] = histc(1:maxd,bins);
cd(1:maxd,1) = cdb(binId);

% Shorten the bins array to be the same length as cdb
idx = find(cdb > 0, 1, 'last');
cdb = cdb(1:idx);
bins = bins(1:idx);


% Create info
info.nsamples = nsamples;
info.gcc = gcc;
info.bins = bins;
info.cc_per_bin = cdb;
info.deg_per_vertex = d;
info.wedges_per_vertex = w;
info.tris_per_vertex = t;
info.wedges_per_bin = binWedges;
info.tris_per_bin = binTriangles;

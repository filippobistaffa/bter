function s = random_sample(cnts, nsamples)
%RANDOM_SAMPLE creates a random sample proportional to the given counts.
%
%   S = RANDOM_SAMPLE(C) choose N = round(sum(C)) samples (with
%   replacement) from {1,...,length(C)} proportional to the values in C.
%   So, if C = [2 1 1], then we might expect S (sorted) to be [ 1 1 2 3 ].
%   However, we also allow for C to be non-integral.
%
%FEASTPACK v1.0, Sandia National Laboratories, April 2013.

% Sandia National Laboratories is a multi-program laboratory managed and
% operated by Sandia Corporation, a wholly owned subsidiary of Lockheed
% Martin Corporation, for the U.S. Department of Energy's National Nuclear
% Security Administration under contract DE-AC04-94AL85000. 

if ~exist('nsamples','var')
    nsamples = round(sum(cnts));
else
    cnts = cnts .* nsamples / sum(cnts);
end

cumdist = [0; cumsum(cnts)];
bins = cumdist / cumdist(end);

testval = abs(bins(end) - 1);
if  testval > eps
    warning('Last entry of bins is not exactly 1. Diff = %e.', testval);
end

[~, s] = histc(rand(nsamples,1),bins);
function p = dglnpdf(n,alpha,beta)
%DGLNPDF Discrete generalized log-normal probability density function.
%
%   P = DGLNPDF(N,ALPHA,BETA) returns the probabilities for a discrete
%   version of the generalized log-normal probability density function. In
%   this case, Prob(x) ~ exp(-(log(x)/alpha)^beta) for x = 1:N.
%
%   See also DPLPDF, GENDEGDIST.
%
%FEASTPACK v1.0, Sandia National Laboratories, November 2012

% Sandia National Laboratories is a multi-program laboratory managed and
% operated by Sandia Corporation, a wholly owned subsidiary of Lockheed
% Martin Corporation, for the U.S. Department of Energy's National Nuclear
% Security Administration under contract DE-AC04-94AL85000. 


p = exp(-((log((1:n)'))/alpha).^beta);
p = p / sum(p);

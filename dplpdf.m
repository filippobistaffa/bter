function p = dplpdf(n,gamma)
%DPLPDF Discrete power law probability density function.
%
%   P = DPLPDF(N,GAMMA) returns the probabilities for a discrete
%   version of the power law probability density function. In
%   this case, Prob(x) ~ x^(-gamma) for x = 1:N.
%
%   See also DGLNPDF, GENDEGDIST.
%
%FEASTPACK v1.0, Sandia National Laboratories, Feburary 2013.

% Sandia National Laboratories is a multi-program laboratory managed and
% operated by Sandia Corporation, a wholly owned subsidiary of Lockheed
% Martin Corporation, for the U.S. Department of Energy's National Nuclear
% Security Administration under contract DE-AC04-94AL85000. 

p = (1:n)'.^(-gamma);
p = p / sum(p);

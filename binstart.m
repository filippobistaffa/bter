function idx = binstart(i, omega, tau, idx0)
%BINSTART - Specify start of bin for the specified parameters.
%
%   K = BINSTART(I,OMEGA,TAU,K0) returns the index of the I-th bin defined
%   by parameters TAU, OMEGA, and K0. The parameters TAU, OMEGA, and K0 are
%   optional. The default values are OMEGA=2, TAU=1, K0=1.
%
%   The end of a bin I one less than the end of the next bin, i.e., 
%   KEND = BINSTART(I+1,OMEGA,TAU,K0)-1.
%
%   See also BINLOOKUP, BINDATA.
%
%FEASTPACK v1.0, Sandia National Laboratories, 2012.

% Sandia National Laboratories is a multi-program laboratory managed and
% operated by Sandia Corporation, a wholly owned subsidiary of Lockheed
% Martin Corporation, for the U.S. Department of Energy's National Nuclear
% Security Administration under contract DE-AC04-94AL85000. 

% **
if ~exist('omega','var') || isempty(omega)
    omega = 2;
end

if ~exist('tau','var') || isempty(tau)
    tau = 1;
end

if ~exist('idx0','var') || isempty(idx0)
    idx0 = 1;
end

% **
n = length(i);
idx = zeros(n,1);
for k = 1:n
    if i(k) <= tau
        idx(k) = i(k) + idx0 - 1;
    else
        idx(k) = ceil((omega.^(i(k)-tau)-1)/(omega-1)) + tau + idx0 - 1;
    end
end
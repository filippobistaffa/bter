function target = cnt(nnodes, avgdeg_input, avgdeg_target, tol, gcc_target, seed, graphcsv, statscsv)

rng(seed);
maxdeg_bound=1e4;
maxccd_target=0.95;
nedges_target= nnodes*avgdeg_input/2;
%meq=(-sqrt(-4*avgdeg_input*nnodes+4*nnodes^2-4*nnodes+1)+2*nnodes-1)/2;
graphname = 'test';
tau = 1e-3 / nnodes; % tau << 1/nnodes
[alpha, beta] = degdist_param_search(avgdeg_input, maxdeg_bound, 'maxdeg_prbnd', tau);
pdf = dglnpdf(maxdeg_bound,alpha,beta);
nd = gendegdist(nnodes,pdf);
maxdeg = find(nd>0,1,'last');
xi = cc_param_search(nd, maxccd_target, gcc_target);
ccd_target = [0; maxccd_target * exp(-(0:maxdeg-2)'.* xi)];
ccd_stddev = min(0.01,ccd_target/2);
ccd = max(ccd_target +  randn(size(ccd_target)) .* ccd_stddev,0);
[E1,E2] = bter(nd,ccd,'rngseed',seed,'verbose',false);
G_bter = bter_edges2graph(E1,E2);
nnodes_bter = size(G_bter,1);
nedges = sum(nd .* (1:length(nd))');
nedges_bter = nnz(G_bter)/2;
nd_bter = accumarray(nonzeros(sum(G_bter,2)),1);
maxdeg_bter = find(nd_bter>0,1,'last');
[ccd_bter,gcc_bter] = ccperdeg(G_bter);
ad_bter=2*nedges_bter/nnodes_bter;
meq_bter=(-sqrt(-4*ad_bter*nnodes_bter+4*nnodes_bter^2-4*nnodes_bter+1)+2*nnodes_bter-1)/2;

if (nnodes == nnodes_bter && abs(avgdeg_target - ad_bter) < tol)
	% Save edges to file
	[i,j]=find(G_bter);
	fid = fopen(graphcsv,'w');
	for k=1:length(i)
	if i(k) > j(k)
	fprintf(fid, '%d,%d\n', i(k)-1, j(k)-1);
	end
	end
	fclose(fid);
	% Save stats
	fid=fopen(statscsv,'w');
	%fprintf(fid,'%d,%d,%.5f,%.5f,%.5f\n', nnodes, nedges_target, avgdeg_input, meq, gcc_target);
	fprintf(fid,'%d,%d,%d,%.5f,%.5f,%.5f\n', seed, nnodes_bter, nedges_bter, ad_bter, meq_bter, gcc_bter);
	fclose(fid);
	target = 0;
else
	target = avgdeg_target/ad_bter;
end


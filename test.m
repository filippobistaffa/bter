ntest = 5;
nmin = 20;
nstep = 1;
nmax = 50;
admin = 2;
admax = 5;
tol = 0.1;
gcc_target=0.15;
ad_input=15;
folder='output';

for n=nmin:nstep:nmax
	mkdir(folder,num2str(n));
	for ad=admin:admax
		outfolder=fullfile(folder,num2str(n),num2str(ad));
		mkdir(outfolder);
		cnloop(n, ad_input, ad, tol, gcc_target, ntest, outfolder);
	end
end
